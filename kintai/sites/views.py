import os
from django.shortcuts import render

from django.conf import settings
from django.http.response import HttpResponse

# Create your views here.
def index(request):
    return render(request, 'sites/index.html')

def qrmaker(request):
    return render(request, 'sites/qrmaker.html')
