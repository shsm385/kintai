from django.conf.urls import include, url
from django.urls import path
from rest_framework import routers
from . import views
from sites.views import index
from sites import views as qr


urlpatterns = [
    path('qrmaker/', qr.qrmaker , name='qrmaker'),
    path('', views.index, name='index'),
]
